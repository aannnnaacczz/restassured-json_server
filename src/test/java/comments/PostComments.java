package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;

public class PostComments {

    final String COMMENTS_URL = "http://localhost:3000/comments";

    @Test
    void shouldReturnCode201WhenNewCommentIsCreated(){
        Map<String,Object> postBody = new HashMap<>();
        postBody.put("body", "body comment");
        postBody.put("postID", 101);

        given().contentType(ContentType.JSON).
        and().body(postBody).
        when().post(COMMENTS_URL).
        then().statusCode(201);


    }


}
//    {
//            "id": 2222,
//            "body": "comment",
//            "postId": 1
//            }