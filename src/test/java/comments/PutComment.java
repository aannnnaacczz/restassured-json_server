package comments;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.is;

public class PutComment {
    final String COMMENTS_URL = "http://localhost:3000/comments";

    @Test
    void shouldReturnCode201WhenCommentIsUpdated() {
        Map<String, Object> putBody = new HashMap<>();
        putBody.put("body", "comment");
        putBody.put("postID", 101);

        given().contentType(ContentType.JSON).
                and().body(putBody).
                when().put(COMMENTS_URL+"/101").
                then().statusCode(200).
                and().body("postID", is(101));
    }
}