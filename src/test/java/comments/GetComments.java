package comments;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.is;

public class GetComments {

    final String COMMENTS_URL = "http://localhost:3000/comments";

    @Test
    void shouldReturnStatus200WhenGettingAllComments() {
                when().get(COMMENTS_URL).
                then().statusCode(200);
    }

    @Test
    void shouldReturnCorrectBodyStructureWhenGettingSingleComment() {
                given().pathParam("id", 1).
                when().get(COMMENTS_URL + "/{id}").
                then().body("id", is(1)).
                and().body("postId", is(1)).
                and().body("body", is("some comment1"));
    }

}
